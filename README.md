# Original source : 

Before fork you were able to get that kit from [https://github.com/GJKJ/WSKS](https://github.com/GJKJ/WSKS).

# Why making it a fork :

- No more supported by author(s) _archived_

- Bad programming _not platform independent (windows only)_

- Bad documentation _not even a wiring schematic_

- New [Documentation](./doc/Guide.md)

![schema](./doc/schema.png)

![PCB_copper](./doc/PCB_copper.png)

![PCB_implantation](./doc/Implantation.png)

![PCB_connectors](./doc/PCB.png)

# Goals :

- improve code

- give a real wiring schematic 

- might become a PCB kit to plug that all around esp8266 Board.

- might become a PCB kit to plug that all around also a arduino pro mini.

# LICENSE : 

- my version will use unlicensed [license](./LICENSE) as far as I use a bunch of the forked code.

- All untouched files remains using their original license & developers are still responsible of it all, even on damages to your devices.

# production files

[kicad work files](./kicad/)

[Gerber files](./Gerberfiles/) ready to send to PCB manufacturer.
