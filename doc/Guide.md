﻿
**Weather Station Guide Manual**

# **Features：**
-  Get weather data from OpenWeathermap, showing the weather of today and the weather forecast for the next 3 days in any city in the world

-  Read the current temperature, humidity

-  Read atmospheric pressure and light intensity

-  Upload temperature, humidity,  atmospheric pressure and light intensity data to thingspeak.com at regular intervals

-  View weather forecast on Display and view environmental monitoring chart on thingspeak.com

# **Wiring Diagram：**

![wirering](./wire.png)

# **Install Arduino IDE for ESP8266**
Only Arduino IDE version 1.8.4 is recommended,other version may can’t work with the weather station kits source code.

Arduino IDE 1.8.4 download URL:  

<https://www.amazon.com/clouddrive/share/5hYUsaMRYFatPBizWinTapxlSzqdW8LQtVyiihAYIS5> 

Open Arduino IDE

File > Preferences

Insert the following link in the "Additional Boards Manager URLs:" text box:：

<http://arduino.esp8266.com/stable/package_esp8266com_index.json>

-  Click "OK" to close the dialog.

-  Go to Tools > Board > Board Manager and click this option

- Type "ESP8266" in the text box, search, and display an option "ESP8266 by ESP8266 Community"

-  Click on "more info" and then click on the "Install" button

-  After the installation process is complete (may take 1 minute or 2 minutes), you can close the dialog by pressing the "Close" button

-  Go back to Tools > Boards and you should list some "new" boards at the bottom of the list. Select "NodeMCU 1.0 (ESP-12E Module)"

-  Choose the correct port:

## **Connecting Components - Pins Map**

|ESP8266-12E|OLED|
| :-: | :-: |
|3.3V|VCC|
|GND|GND|
|D3|SDA|
|D4|SCL|

|ESP8266-12E|DHT11|
| :-: | :-: |
|3.3V|VCC|
|GND|GND|
|D5|DATA|
|||

|ESP8266-12E|BH1750FVI|
| :-: | :-: |
|3.3V|VCC|
|GND|GND|
|D3|SDA|
|D4|SCL|

|ESP8266-12E|BMP180|
| :-: | :-: |
|3.3V|VCC|
|GND|GND|
|D3|SDA|
|D4|SCL|

|ESP8266-12E|Computer|
| :-: | :-: |
|Micro-USB|USB|

# **External work: Register OpenWeathermap, thingspeak new account**

- Sign up new account at <https://home.openweathermap.org/api_keys> to get the API Keys for openweathermap:

- Sign up for a new account at <https://thingspeak.com> 

- Create a new Channel and receive the temperature, humidity, light and atmosphere data from ESP8266-12E(WeatherStation)

- Set Channel to Public

- Get the API\_keys of the channle

- You can view the updated data later
